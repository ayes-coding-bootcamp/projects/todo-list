import {createGlobalStyle} from "styled-components";
import {ThemeType} from "./data/themes";

const GlobalStyle = createGlobalStyle`
  html, body {
	font-family: Arial, sans-serif;
	font-size: 20px;
	overflow-x: hidden;
	scroll-behavior: smooth;
	background-color: ${({theme}: { theme: ThemeType }) => theme.layer0.background};
	color: ${({theme}: { theme: ThemeType }) => theme.layer0.color};
	transition: all 0.3s, color 0.3s;
  }
  
  body {
	min-height: 100vh;
  }

  * {
	box-sizing: border-box;
  }
`;

export default GlobalStyle;
