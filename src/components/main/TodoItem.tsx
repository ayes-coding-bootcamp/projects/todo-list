import React, {Dispatch, SetStateAction, useState} from "react";
import {CiCircleRemove} from "react-icons/ci";
import styled from "styled-components";
import {ThemeType} from "../../data/themes";

interface TodoItemProps {
	id: string;
	title: string;
	todoItems: { id: string; title: string; isExiting?: boolean }[];
	setTodoItems: Dispatch<SetStateAction<{ id: string; title: string; isExiting?: boolean }[]>>;
}

const StyledTodoItem = styled.article<{ isChecked: boolean, theme: ThemeType }>`
  background: ${({theme}) => theme.layer1.background};
  padding: 1rem;
  border-radius: 12px;
  display: flex;
  align-items: center;
  margin: 12px 0;
  gap: 16px;

  label {
	flex: 0 0 20px;
	aspect-ratio: 1 / 1;
	width: auto;
	height: auto;
	background: transparent;
	outline: 2px solid ${({theme}) => theme.layer1.color};
	outline-offset: 2px;
	border-radius: 10%;

	transition: all 200ms;

	${({isChecked, theme}: {
	  isChecked: boolean,
	  theme: ThemeType
	}) => isChecked && `background: ${theme.layer1.color};`}
	&:hover {
	  outline: 2px solid ${({theme}) => theme.hovered};
	}


	input[type="checkbox"] {
	  position: absolute;
	  width: 0;
	  height: 0;
	  opacity: 0;
	}
  }

  h2 {
	margin: 0;
	overflow-x: hidden;
	text-overflow: ellipsis;
	text-decoration: ${({isChecked}) => (isChecked ? "line-through" : "none")};
	font-size: 1.3rem;
  }

  button {
	aspect-ratio: 1 / 1;
	flex: 0 0 50px;
	color: inherit;
	background: none;
	border: none;
	border-radius: 99999px;
	margin-left: auto;

	transition: all 200ms;

	svg {
	  color: inherit;
	  width: 100%;
	  height: auto;
	}

	&:hover {
	  color: ${({theme}) => theme.hovered};
	}
  }
`;

const TodoItem: React.FC<TodoItemProps> = ({id, title, setTodoItems}) => {
	const [isChecked, setIsChecked] = useState(false);

	const removeItem = () => {
		setTodoItems((prevItems) => prevItems.filter((item) => item.id !== id));
	};

	return (
		<StyledTodoItem isChecked={isChecked}>
			<label htmlFor={id}>
				<input type="checkbox" id={id} checked={isChecked} onChange={() => setIsChecked(!isChecked)}/>
			</label>
			<h2>{title}</h2>
			<button onClick={removeItem}>
				<CiCircleRemove/>
			</button>
		</StyledTodoItem>
	);
};

export default TodoItem;
