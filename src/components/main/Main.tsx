import React, {useState} from "react";
import {Flipped, Flipper, spring} from "react-flip-toolkit";
import styled from "styled-components";
import {v4 as uuidv4} from "uuid";
import {ThemeType} from "../../data/themes";
import {hexToRgb} from "../../utils/conversion";
import TodoItem from "./TodoItem";


interface MainProps {
	theme: ThemeType;
}

type CustomSpringValues = {
	translateY: number;
	opacity: number;
};

function isCustomSpringValues(values: unknown): values is CustomSpringValues {
	return (values as CustomSpringValues).translateY !== undefined && (values as CustomSpringValues).opacity !== undefined;
}

/* Style */
const StyledMain = styled.main<MainProps>`
  form {
	display: flex;
	flex-direction: column;
	align-items: center;

	input {
	  display: block;

	  color: ${props => {
		const {r, g, b} = hexToRgb(props.theme.layer0.color);
		return `rgba(${r}, ${g}, ${b}, 1)`;
	  }};
	  background: ${props => {
		const {r, g, b} = hexToRgb(props.theme.layer0.color);
		return `rgba(${r}, ${g}, ${b}, 0.05)`;
	  }};
	  border: 2px solid ${props => {
		const {r, g, b} = hexToRgb(props.theme.layer0.color);
		return `rgba(${r}, ${g}, ${b}, 0.2)`;
	  }};
	  width: clamp(150px, 75%, 600px);
	  padding: 0.5rem 1rem;
	  border-radius: 99999px;
	  font-family: Arial, sans-serif;
	  font-size: 1rem;
	  margin: 1rem 0;
	  outline: 2px solid transparent;

	  transition: outline 250ms, border 250ms;

	  &:focus {
		outline: 2px solid ${props => {
		  const {r, g, b} = hexToRgb(props.theme.focus);
		  return `rgba(${r}, ${g}, ${b}, 0.8)`;
		}};
		border: 2px solid transparent;
	  }
	}
  }

  section {
	padding: 1rem;
	margin: 0 auto;
	max-width: 1000px;
  }
`;

/* Component */
const Main: React.FC<MainProps> = ({theme}) => {
	const [inputItem, setInputItem] = useState("");
	const [todoItems, setTodoItems] = useState<{ id: string; title: string }[]>([]);

	const addItem = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const newId = uuidv4();
		setTodoItems([{id: newId, title: inputItem}, ...todoItems]);
		setInputItem("");
	};

	const removeItem = (id: string) => setTodoItems((prevItems) => prevItems.filter((item) => item.id !== id));

	const onAppear = (el: HTMLElement, index: number) => {
		spring({
			config: "wobbly",
			values: {
				translateY: [-15, 0],
				opacity: [0, 1]
			},
			onUpdate: (values: unknown) => {
				if (isCustomSpringValues(values)) {
					el.style.opacity = values.opacity.toString();
					el.style.transform = `translateY(${values.translateY}px)`;
				}
			},
			delay: index * 25,
		});
	}

	const onExit = (el: HTMLElement, index: number, removeElement: () => void, id: string) => {
		removeItem(id);

		spring({
			config: "wobbly",
			values: {
				translateY: [0, -15],
				opacity: [1, 0]
			},
			onUpdate: (values: unknown) => {
				if(isCustomSpringValues(values)){
					el.style.opacity = values.opacity.toString();
					el.style.transform = `translateY(${values.translateY}px)`;
				}
			},
			delay: index * 25,
			onComplete: removeElement
		});
	};

	return (
		<StyledMain theme={theme}>
			<form onSubmit={addItem}>
				<input
					type="text"
					placeholder="Add an item"
					onChange={(event) => setInputItem(event.target.value)}
					value={inputItem}
				/>
			</form>
			<section>
				{/* Replace the existing map function with the one using transitions */}
				<Flipper flipKey={JSON.stringify(todoItems)}>
					{todoItems.map((item) => (
						<Flipped
							key={item.id}
							flipId={item.id}
							stagger="true"
							onAppear={(el, index) => onAppear(el, index)}
							onExit={(el, index, removeElement) => onExit(el, index, removeElement, item.id)}

						>
							<div>
								<TodoItem
									title={item.title}
									todoItems={todoItems}
									setTodoItems={setTodoItems}
									id={item.id}
								/>
							</div>
						</Flipped>
					))}
				</Flipper>
			</section>
		</StyledMain>
	);
};

export default Main;
