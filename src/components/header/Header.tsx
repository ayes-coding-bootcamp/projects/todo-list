import React from "react";
import {FaRegCalendarCheck} from "react-icons/fa";
import {TbMoonStars, TbSun} from "react-icons/tb";
import styled from "styled-components";
import {darkTheme, lightTheme} from "../../data/themes";

/* Type */
interface HeaderProps {
	theme: typeof darkTheme;
	setTheme: (theme: typeof darkTheme) => void;
}


/* Style */
const StyledHeader = styled.header`
  button {
	border-radius: 99999px;
	color: inherit;
	background: none;
	border: none;

	svg {
	  color: inherit;
	  width: 100%;
	  height: auto;
	}
  }
`;

/* Component */
const Header: React.FunctionComponent<HeaderProps> = (props) => {
	return (
		<StyledHeader>
			<article>
				<FaRegCalendarCheck/>
				<h1>
					Your To-Do List
				</h1>
			</article>
			<button onClick={() => {props.setTheme(props.theme.name === "dark" ? lightTheme : darkTheme);}}>
				{props.theme.name === "light" ? <TbMoonStars/> : <TbSun/>}
			</button>
		</StyledHeader>
	);
};

export default Header;
