import "styled-components";
import {ThemeType} from "./data/themes";

declare module "styled-components" {
	export interface DefaultTheme extends ThemeType {
	}
}
