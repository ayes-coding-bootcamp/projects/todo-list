export interface ThemeType {
	name: string;
	focus: string;
	hovered: string;
	layer0: {
		background: string,
		color: string,
	};
	layer1: {
		background: string,
		color: string
	};
}

export const lightTheme: ThemeType = {
	name: "light",
	focus: "#ff5a1a",
	hovered: "#5397be",
	layer0: {
		background: "#dcdcdc",
		color: "#0a0a0a"
	},
	layer1: {
		background: "#cecece",
		color: "#262626"
	}
};

export const darkTheme: ThemeType = {
	name: "dark",
	focus: "#ff5a1a",
	hovered: "#5397be",
	layer0: {
		background: "#0a0a0a",
		color: "#dcdcdc"
	},
	layer1: {
		background: "#262626",
		color: "#cecece"
	}
};
