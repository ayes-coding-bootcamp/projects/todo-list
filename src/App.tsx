import React, {useState} from "react";
import styled, {ThemeProvider} from "styled-components";
import Header from "./components/header/Header";
import Main from "./components/main/Main";
import {darkTheme} from "./data/themes";
import GlobalStyle from "./GlobalStyle";

const StyledApp = styled.div`
  margin: 0 auto;
  padding: 1rem;
  min-width: 320px;

  header {
	display: flex;
	padding: 1rem;

	article {
	  display: flex;
	  align-items: center;
	  justify-content: center;
	  gap: clamp(1rem, 10%, 15rem);
	  flex: 1 1 100%;

	  text-align: center;

	  h1 {
		margin: 0;
	  }

	  svg {
		width: 50px;
		height: auto;
	  }
	}

	button {
	  margin-left: auto;
	  width: 50px;
	  height: auto;
	  flex: 0 0 50px;
	}
  }
`;

function App() {
	const [theme, setTheme] = useState(darkTheme);
	return (
		<ThemeProvider theme={theme}>
			<GlobalStyle/>
			<StyledApp>
				<Header theme={theme} setTheme={setTheme}/>
				<Main theme={theme}/>
			</StyledApp>
		</ThemeProvider>
	);
}

export default App;
